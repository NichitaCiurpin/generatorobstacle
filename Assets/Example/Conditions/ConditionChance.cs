﻿using UnityEngine;
using UnityEditor;

public class ConditionChance : BaseCondition
{
    private int _chanceOfTrue = 0;

    public override void DrawInInspector()
    {
        EditorGUILayout.BeginHorizontal();
        _chanceOfTrue = EditorGUILayout.IntField("Chance Of True",_chanceOfTrue);
        _chanceOfTrue = Mathf.Clamp(_chanceOfTrue, 0, 100);
        EditorGUILayout.EndHorizontal();
    }

    public override bool IsConditionPass()
    {
        int randomInt = UnityEngine.Random.Range(0, 100);
        return randomInt <= _chanceOfTrue;
    }
}

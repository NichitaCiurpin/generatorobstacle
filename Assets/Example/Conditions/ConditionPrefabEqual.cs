﻿using UnityEngine;
using UnityEditor;

public class ConditionPrefabEqual : BaseCondition
{
    private GameObject _prefab ;

    public override bool IsConditionPass()
    {
        bool isPrefabEqual = _prefab.Equals(GeneratorFSMData.Result.Prefab);
        return isPrefabEqual;
    }

    public override void DrawInInspector()
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Is result prefab equals to ",GUILayout.Width(150));

        _prefab = EditorGUILayout.ObjectField(_prefab, typeof(GameObject), true) as GameObject;

        EditorGUILayout.EndHorizontal();
    }
}

﻿using UnityEngine;
using UnityEditor;

public class ConditionBool : BaseCondition
{
    private bool _isTransition;

    public override bool IsConditionPass()
    {
        return _isTransition;
    }

    public override void DrawInInspector()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Make Transition", GUILayout.Width(100));
        _isTransition = EditorGUILayout.Toggle(_isTransition);
        EditorGUILayout.EndHorizontal();
    }
}

﻿using UnityEngine;
using UnityEditor;

public class ModificatorColor : BaseModificator
{
    [SerializeField]
    private Color _color = Color.white;

    public override void Modify()
    {
        GeneratorFSMData.Result.Color = this._color;
    }

    public override void DrawModificator()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Color to apply");
        _color = EditorGUILayout.ColorField(_color);
        EditorGUILayout.EndHorizontal();
    }
}

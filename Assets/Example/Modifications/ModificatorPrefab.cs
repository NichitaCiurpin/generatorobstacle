﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class ModificatorPrefab : BaseModificator
{
    [SerializeField]
    private List<GameObject> _listPrefabs = new List<GameObject>();

    public List<GameObject> ListPrefabs
    {
        get
        {
            return _listPrefabs;
        }
    }

    public override void Modify()
    {
        if (_listPrefabs.Count == 0) return;

        int rand = Random.Range(0, _listPrefabs.Count);
        GeneratorFSMData.Result.Prefab = ListPrefabs[rand];
    }

    public override void DrawModificator()
    {
        for(int i = 0; i < _listPrefabs.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            _listPrefabs[i] = EditorGUILayout.ObjectField(_listPrefabs[i], typeof(GameObject), true) as GameObject;
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("+", GUILayout.Width(20)))
        {
            _listPrefabs.Add(null);
        }
    }
}

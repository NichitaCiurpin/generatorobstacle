﻿using UnityEngine;
using UnityEditor;

public class ModificatorScale : BaseModificator
{
    [SerializeField]
    private float _scaleModifier = 1;

    public override void Modify()
    {
        float temp = _scaleModifier;
        GeneratorFSMData.Result.Scale = new Vector3(temp, temp, temp);
    }

    public override void DrawModificator()
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Scale modifier");
        _scaleModifier = EditorGUILayout.FloatField(_scaleModifier);

        EditorGUILayout.EndHorizontal();
    }
}

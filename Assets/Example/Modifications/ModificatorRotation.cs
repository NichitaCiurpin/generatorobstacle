﻿using UnityEngine;
using UnityEditor;

public class ModificatorRotation : BaseModificator
{
    [SerializeField]
    private bool _isXAxisRotate = false;
    [SerializeField]
    private bool _isYAxisRotate = false;
    [SerializeField]
    private bool _isZAxisRotate = false;
    [SerializeField]
    private Vector3 _rotation = Vector3.zero;

    public override void Modify()
    {
        if (_isXAxisRotate)
        {
            _rotation.x = UnityEngine.Random.Range(0, 360);
        }
            
        if (_isYAxisRotate)
        {
            _rotation.y = UnityEngine.Random.Range(0, 360);
        }
            
        if (_isZAxisRotate)
        {
            _rotation.z = UnityEngine.Random.Range(0, 360);
        }

        GeneratorFSMData.Result.Rotation = _rotation;
    }

    public override void DrawModificator()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Rotate around X Axis");
        _isXAxisRotate = EditorGUILayout.Toggle(_isXAxisRotate);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Rotate around Y Axis");
        _isYAxisRotate = EditorGUILayout.Toggle(_isYAxisRotate);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Rotate around Z Axis");
        _isZAxisRotate = EditorGUILayout.Toggle(_isZAxisRotate);
        EditorGUILayout.EndHorizontal();
    }
}

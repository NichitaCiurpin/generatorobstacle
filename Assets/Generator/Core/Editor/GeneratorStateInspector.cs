﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using Arbor;


[CustomEditor(typeof(GeneratorState), true)]
public class GeneratorStateInspector : Editor
{
    private GeneratorState _inspectedObject;
    private List<BaseCondition> _listCondition;
    private bool _itemListWasDeleted;

    const string _pathToCreateAsset = "Assets/Generator/";
    const string _assetType = ".asset";
    const string _baseModificatorType = "BaseModificator";
    const string _baseConditionType = "BaseCondition";

    public override void OnInspectorGUI()
    {
        _itemListWasDeleted = false;

        _inspectedObject = target as GeneratorState;

        ManageGeneratorFSM.DeleteLostModificators(_inspectedObject.stateMachine);

        ChangeTransitionName();

        DrawScriptableObject();

        if (_inspectedObject.BaseModificator == null)
            return;

        using (new Surround())
        {
            DrawModification();
        }

        DrawTransition();

        EditorUtility.SetDirty(_inspectedObject.BaseModificator);
        Repaint();
    }

    private void ChangeTransitionName()
    {
        BaseModificator baseModificator = _inspectedObject.BaseModificator;
        if (baseModificator == null)
            return;

        List<StateLink> listStateLink = _inspectedObject.ListStateLink;

        for (int i = 0; i < listStateLink.Count; i++)
        {
            if (listStateLink[i].stateID != 0)
            {
                State temp = _inspectedObject.state.stateMachine.GetStateFromID(listStateLink[i].stateID);
                listStateLink[i].name = temp.name;
                if (baseModificator.ListTransition.Count != 0)
                {
                    baseModificator.ListTransition[i].Link.name = temp.name;
                }
            }
        }
    }

    private void DrawScriptableObject()
    {
        EditorGUILayout.BeginHorizontal();

        if (_inspectedObject.BaseModificator == null)
        {
            EditorGUILayout.LabelField("Null");
        }
        else
        {
            Type temp = _inspectedObject.BaseModificator.GetType();
            EditorGUILayout.LabelField(temp.ToString());
        }

        if (Button("Sellect",50))
        {
            ChangeModifier();
        }

        EditorGUILayout.EndHorizontal();
    }

    private void DrawModification()
    {
        _inspectedObject.BaseModificator.DrawModificator();
    }

    private void DrawTransition()
    {
        List<Transition> listTransition = _inspectedObject.BaseModificator.ListTransition;

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Transition ", GUILayout.Width(255));

        if (Button("+",20))
        {
            AddTransition();
        }

        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < listTransition.Count; i++)
        {
            using (new Surround())
            {
                EditorGUILayout.BeginHorizontal();

                string name = _inspectedObject.BaseModificator.ListTransition[i].Link.name;
                listTransition[i].IsShowFoldout = EditorGUILayout.Foldout(listTransition[i].IsShowFoldout, name + " ");

                if (Button("-",20))
                {
                    RemoveTransition(i);
                }

                if (_itemListWasDeleted)
                    return;

                EditorGUILayout.EndHorizontal();

                if (listTransition[i].IsShowFoldout)
                {
                    using (new Surround())
                    {
                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("Condition", GUILayout.Width(240));

                        if (Button("+",20))
                        {
                            AddCondition(i);
                        }

                        EditorGUILayout.EndHorizontal();

                        List<BaseCondition> listBaseCondition = listTransition[i].ListCondition;

                        for (int j = 0; j < listBaseCondition.Count; j++)
                        {
                            using (new Surround())
                            {
                                EditorGUILayout.BeginHorizontal();

                                if (listBaseCondition[j] != null)
                                {
                                    EditorGUILayout.LabelField(listBaseCondition[j].GetType().ToString(), GUILayout.Width(235));
                                }

                                if (Button("-",20))
                                {
                                    RemoveCondition(i, j);
                                }

                                if (_itemListWasDeleted)
                                    return;

                                EditorGUILayout.EndHorizontal();

                                EditorGUILayout.BeginHorizontal();

                                if (listBaseCondition[j] != null)
                                {
                                    listBaseCondition[j].DrawInInspector();

                                    EditorUtility.SetDirty(listBaseCondition[j]);
                                }

                                EditorGUILayout.EndHorizontal();
                            }
                        }
                    }
                }
            }
        }
    }

    private void CallbackCondition(Type conditionToCreate)
    {
        if (_listCondition == null)
            return;
        if (conditionToCreate == null)
            return;

        BaseCondition createdCondition = ScriptableObject.CreateInstance(conditionToCreate) as BaseCondition;

        createdCondition.FsmHash = _inspectedObject.stateMachine.GetHashCode();

        AssetDatabase.AddObjectToAsset(createdCondition, _inspectedObject.BaseModificator);
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(createdCondition));
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        _listCondition.Add(createdCondition);
    }

    private void CallbackModification(Type obj)
    {
        if (obj == null)
            return;

        BaseModificator createdModificator = ScriptableObject.CreateInstance(obj) as BaseModificator;

        createdModificator.FsmHash = _inspectedObject.stateMachine.GetHashCode();

        BaseModificator previousModificator = _inspectedObject.BaseModificator;

        if (previousModificator != null)
        {
            string path = AssetDatabase.GetAssetPath(previousModificator);
            AssetDatabase.DeleteAsset(path);
        }

        _inspectedObject.BaseModificator = createdModificator as BaseModificator;
        _inspectedObject.ListStateLink = new List<StateLink>();

        int increment = 0;
        bool isAssetLoaded = true;

        int warning = 0;
        while (isAssetLoaded)
        {
            warning++;
            if (warning == 500)
            {
                Debug.LogError("WhileLoop");
                break;
            }

            increment++;
            isAssetLoaded = AssetDatabase.IsMainAssetAtPathLoaded(_pathToCreateAsset + obj.ToString() + " " + increment + _assetType);
        }

        string path2 = _pathToCreateAsset + obj.ToString() + " " + increment + _assetType;

        AssetDatabase.CreateAsset(createdModificator, path2);
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(createdModificator));
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        EditorUtility.SetDirty(_inspectedObject);
    }

    private void ChangeModifier()
    {
        IEnumerable<Type> typesOfBaseModificator = ReflectionHelper.FindDerivedTypes(Assembly.GetAssembly(typeof(BaseModificator)), typeof(BaseModificator));
        GenericMenu menu = new GenericMenu();

        foreach (var value in typesOfBaseModificator)
        {
            if (value.Name == _baseModificatorType)
            {
                continue;
            }

            menu.AddItem(new GUIContent(value.ToString()), false, (obj) => { CallbackModification(value); }, value.ToString());
        }

        menu.ShowAsContext();
    }

    private bool Button(string text, int wight)
    {
        return GUILayout.Button(text, GUILayout.Width(wight));
    }

    private void AddTransition()
    {
        List<Transition> listTransition = _inspectedObject.BaseModificator.ListTransition;
        Transition newTransition = new Transition(_inspectedObject);
        newTransition.Link.name = "Transition " + (listTransition.Count + 1);
        listTransition.Add(newTransition);
    }

    private void RemoveTransition(int i)
    {
        List<Transition> listTransition = _inspectedObject.BaseModificator.ListTransition;
        List<BaseCondition> listCondition = listTransition[i].ListCondition;

        for (int j = 0; j < listCondition.Count; j++)
        {
            DestroyImmediate(listCondition[j], true);
            AssetDatabase.SaveAssets();
        }

        listTransition.RemoveAt(i);
        List<StateLink> listStateLink = _inspectedObject.ListStateLink;
        listStateLink.RemoveAt(i);
        for (int j = 0; j < listStateLink.Count; j++)
        {
            listStateLink[j].name = "Transition " + (j + 1);
        }

        _itemListWasDeleted = true;
        return;
    }

    private void AddCondition(int i)
    {
        List<Transition> listTransition = _inspectedObject.BaseModificator.ListTransition;

        GenericMenu menu = new GenericMenu();
        _listCondition = listTransition[i].ListCondition;

        IEnumerable<Type> typesOfBaseConditions = ReflectionHelper.FindDerivedTypes(Assembly.GetAssembly(typeof(BaseCondition)), typeof(BaseCondition));
        foreach (var value in typesOfBaseConditions)
        {
            if (value.Name == _baseConditionType)
                continue;

            menu.AddItem(new GUIContent(value.ToString()), false, (obj) => { CallbackCondition(value); }, value.ToString());
        }

        menu.ShowAsContext();
    }

    private void RemoveCondition(int i, int j)
    {
        List<BaseCondition> listBaseCondition = _inspectedObject.BaseModificator.ListTransition[i].ListCondition;

        if (GUILayout.Button("-", GUILayout.Width(20)))
        {
            DestroyImmediate(listBaseCondition[j], true);
            AssetDatabase.SaveAssets();

            listBaseCondition.RemoveAt(j);
            _itemListWasDeleted = true;

            return;
        }
    }
}


 

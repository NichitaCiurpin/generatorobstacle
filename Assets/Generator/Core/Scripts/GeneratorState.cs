﻿using System.Collections.Generic;
using UnityEngine;
using Arbor;
using System;

public class GeneratorState : StateBehaviour
{
    [SerializeField]
    private BaseModificator _baseModificator;
    [SerializeField]
    private List<StateLink> _listStateLink;

    public BaseModificator BaseModificator
    {
        get
        {
            return _baseModificator;
        }
        set
        {
            _baseModificator = value;
        }
    }

    public List<StateLink> ListStateLink
    {
        get
        {
            return _listStateLink;
        }
        set
        {
            _listStateLink = value;
        }
    }

    public EventHandler FSMDone;

    private void OnFSMDone()
    {
        if (FSMDone != null)
        {
            FSMDone.Invoke(this, EventArgs.Empty);
        }
    }

    public override void  OnStateBegin()
    {
        if (GeneratorFSMData.StartState == null)
        {
            GeneratorFSMData.StartState = this.state;
        }

        if(_baseModificator != null)
        {
            _baseModificator.Modify();
            CheckTransition();
        }
    }

    private void OnGUI()
    {
        for(int i = 0; i < _listStateLink.Count; i++)
        {
            if(_listStateLink[i].stateID != 0)
            {
                State temp = state.stateMachine.GetStateFromID(_listStateLink[i].stateID);
                _listStateLink[i].name = temp.name;
            }
        }
    }

    public void CheckTransition()
    {
        int count = _baseModificator.ListTransition.Count;
        for (int i = 0; i < count; i++)
        {
            bool isPass = _baseModificator.ListTransition[i].CheckConditions();
            if (!isPass)
                continue;

            for (int j = 0; j < _listStateLink.Count; j++)
            {
                if (_listStateLink[j].name != _baseModificator.ListTransition[i].Link.name)
                    continue;

                if (_listStateLink[j].stateID == 0)
                {
                    SaveResult();
                    
                    return;
                }

                Transition(_listStateLink[j]);
                return;
            }
        }
        SaveResult();
    }

    public void SaveResult()
    {
        OnFSMDone();
        GeneratorFSMData.ClearResultData();
    }
}

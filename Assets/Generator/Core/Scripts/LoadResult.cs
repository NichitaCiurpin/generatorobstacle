﻿using UnityEngine;
using Arbor;

[RequireComponent(typeof(ArborFSM), typeof(FSMController))]
public class LoadResult : MonoBehaviour
{
    private FSMController _fSMControler;
    private Transform _spawnPoint;

    [SerializeField]
    private float _intervalTime = 3;
    [SerializeField]
    private float _difficultyIntervalTime = 5;

    private float _lastGenerateTime = 0;
    private float _lastDifficultyTime = 0;

    private void Awake()
    {
        _fSMControler = GetComponent<FSMController>();
        _spawnPoint = GetComponent<FSMController>().gameObject.transform;

        _fSMControler.FSMDoneArgs += FSMDoneArgsHandler;
    }

    private void Update()
    {
        if(_lastGenerateTime + _intervalTime< Time.time)
        {
            _lastGenerateTime = Time.time;

            _fSMControler.Generate();
            Debug.Log(Time.time);
        }

        if (_lastDifficultyTime + _difficultyIntervalTime < Time.time)
        {
            _lastDifficultyTime = Time.time;

            GeneratorFSMData.Difficulty.Level++;
        }
    }

    private void FSMDoneArgsHandler(object sender, FSMDoneEventArgs e)
    {
        var result = e.Result;
        if (result.Prefab == null)
            return;

        GameObject instantiated = Instantiate(result.Prefab, _spawnPoint.position, Quaternion.identity);

        Destroy(instantiated, 5);

        instantiated.transform.localRotation = Quaternion.Euler(result.Rotation);

        instantiated.transform.localScale = result.Scale;

        Renderer rend = instantiated.GetComponent<Renderer>();
        if (rend != null)
        {
            rend.material.color = result.Color;
        }
        else
        {
            Renderer[] renders = instantiated.GetComponentsInChildren<Renderer>();

            foreach(Renderer rend2 in renders)
            {
                rend2.material.color = result.Color;
            }
        }
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 150, 100), "Difficulty " + GeneratorFSMData.Difficulty.Level);
    }
}

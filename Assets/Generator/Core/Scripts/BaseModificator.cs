﻿using System.Collections.Generic;
using UnityEngine;

public abstract class BaseModificator : ScriptableObject
{
    public List<Transition> ListTransition = new List<Transition>();

    public int FsmHash;

    public abstract void Modify();

    public abstract void DrawModificator();
}

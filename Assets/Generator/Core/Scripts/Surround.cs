﻿using System;
using UnityEditor;
using UnityEngine;

public class Surround : IDisposable
{
    public Surround(float offsetFromLeft = 0f, bool displaySurround = true, Color backgroundColor = default(Color), GUIStyle style = default(GUIStyle))
    {
        if (backgroundColor == default(Color))
        {
            EditorGUILayout.BeginVertical(UnityEngine.GUI.skin.box);
        }
        else
        {
            var prevColor = UnityEngine.GUI.backgroundColor;
            UnityEngine.GUI.backgroundColor = backgroundColor;
            UnityEngine.GUI.backgroundColor = prevColor;
        }
    }

    public void Dispose()
    {
        EditorGUILayout.EndVertical();
    }
}

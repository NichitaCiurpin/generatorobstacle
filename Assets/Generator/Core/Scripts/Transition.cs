﻿using System.Collections.Generic;
using Arbor;
using System;
using UnityEngine;

[Serializable]
public class Transition
{
    [SerializeField]
    private string _name = "";

    public List<BaseCondition> ListCondition = new List<BaseCondition>();

    [SerializeField]
    public StateLink Link = new StateLink();

    public bool IsShowFoldout = true;

    public Transition(GeneratorState state)
    {
        state.ListStateLink.Add(Link);
    }

    public bool CheckConditions()
    {
        for (int i = 0; i < ListCondition.Count; i++)
        {
            bool isPass = ListCondition[i].IsConditionPass();
            if (!isPass)
                return false;
        }

        return true;
    }
}
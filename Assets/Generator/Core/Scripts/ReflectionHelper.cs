﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

public static class ReflectionHelper
{
    public static IEnumerable<Type> FindDerivedTypes(Assembly assembly, Type baseType)
    {
        return assembly.GetTypes().Where(t => baseType.IsAssignableFrom(t));
    }
}

﻿using UnityEngine;
using Arbor;

public static class GeneratorFSMData 
{
    public static State StartState;
    public static Result Result = new Result();
    public static Result LastResult = new Result();
    public static Difficulty Difficulty = new Difficulty();

    public static void ClearResultData()
    {
        Result = new Result();
    }
}

public class Result
{
    public GameObject Prefab = null;
    public Vector3 Rotation = Vector3.zero;
    public Vector3 Scale = Vector3.one;
    public Color Color = Color.white;
}

public class Difficulty
{
    [SerializeField]
    public int Level;
}

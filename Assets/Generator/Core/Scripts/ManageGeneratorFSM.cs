﻿using System.Collections.Generic;
using UnityEngine;
using Arbor;
using UnityEditor;

public class ManageGeneratorFSM
{
    public static void DeleteLostModificators(ArborFSMInternal fsm)
    {
        int stateCount = fsm.stateCount;
        List<State> listState = new List<State>();
        for (int i = 0; i < stateCount; i++)
        {
            listState.Add(fsm.GetStateFromIndex(i));
        }

        Object[] baseModificators = Resources.FindObjectsOfTypeAll(typeof(BaseModificator));

        for (int i = 0; i < listState.Count; i++)
        {
            GeneratorState behaviour = listState[i].GetBehaviour<GeneratorState>();
            if (behaviour == null) continue;
            for (int j = 0; j < baseModificators.Length; j++)
            {
                if (behaviour.BaseModificator == null) continue;

                if (behaviour.BaseModificator.Equals(baseModificators[j]))
                {
                    baseModificators[j] = null;
                }
            }
        }

        for (int i = 0; i < baseModificators.Length; i++)
        {
            BaseModificator temp = baseModificators[i] as BaseModificator;

            if (temp == null)
                continue;
            if (temp.FsmHash != fsm.GetHashCode())
                continue;

            if (AssetDatabase.Contains(baseModificators[i]))
            {
                string path = AssetDatabase.GetAssetPath(baseModificators[i]);
                AssetDatabase.DeleteAsset(path);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Arbor;

public class MoveObject : MonoBehaviour 
{
	public GameObject directionPoint;
    
	private float h = 0f;
	private float v = 0f;

	private void Update () 
	{
		MoveUser ();
        ArborFSM fsm = gameObject.GetComponent<ArborFSM>();
	}

	private void MoveUser()
	{
		transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
		h = Input.GetAxis("Mouse X")+h ;
		v = -Input.GetAxis("Mouse Y")+v;

        if (directionPoint == null)
            return;

        transform.position = Vector3.MoveTowards(transform.position, directionPoint.transform.position, 10f);
	}
}



﻿using UnityEngine;
using Arbor;
using System;

[RequireComponent(typeof(ArborFSM),typeof(LoadResult))]
public class FSMController : MonoBehaviour
{
    private ArborFSM _fsm;

    public EventHandler<FSMDoneEventArgs> FSMDoneArgs;

    private void OnFSMDoneArgs()
    {
        if (FSMDoneArgs != null)
        {
            FSMDoneArgs.Invoke(this, new FSMDoneEventArgs(GeneratorFSMData.Result));
        }
    }

    private void Awake()
    {
        _fsm = GetComponent<ArborFSM>();

        for (int i = 0; i < _fsm.stateCount; i++)
        {
            _fsm.GetStateFromIndex(i).GetBehaviour<GeneratorState>().FSMDone += FSMDoneHandler;
        }
    }

    public void Generate()
    {
        _fsm.Transition(GeneratorFSMData.StartState);
    }

    private void FSMDoneHandler(object sender, EventArgs e)
    {
        OnFSMDoneArgs();
    }
}

public class FSMDoneEventArgs : EventArgs
{
    private Result _result;

    public Result Result
    {
        get
        {
            return _result;
        }
        set
        {
            _result = value;
        }
    }

    public FSMDoneEventArgs(Result result)
    {
        Result = result;
    }
}

﻿using UnityEngine;
using Arbor;

public abstract class BaseCondition : ScriptableObject
{
    public int FsmHash;
    
    public abstract bool IsConditionPass();

    public abstract void DrawInInspector();
}

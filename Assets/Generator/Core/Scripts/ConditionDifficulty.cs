﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class ConditionDifficulty : BaseCondition
{
    private enum EqualSign { EQUAL, GREATER, LESS, GREATER_OR_EQUAL, LESS_OR_EQUAL };

    private readonly string[] _options = { "Equal", "Grater", "Less", "GraterOrEqual", "LestOrEqual" };

    [SerializeField]
    private int _difficultyLevel = 0;

    [SerializeField]
    private EqualSign _sign = EqualSign.EQUAL;

    public override void DrawInInspector()
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Difficulty", GUILayout.Width(70));

        _sign = (EqualSign)EditorGUILayout.Popup((int)_sign, _options);

        _difficultyLevel = EditorGUILayout.IntField(_difficultyLevel);

        EditorGUILayout.EndHorizontal();
    }

    public override bool IsConditionPass()
    {
        int currentDifficultyLevel = GeneratorFSMData.Difficulty.Level;

        switch (_sign)
        {
            case EqualSign.EQUAL:
                return currentDifficultyLevel == _difficultyLevel;

            case EqualSign.GREATER:
                return currentDifficultyLevel > _difficultyLevel;

            case EqualSign.LESS:
                return currentDifficultyLevel < _difficultyLevel;

            case EqualSign.GREATER_OR_EQUAL:
                return currentDifficultyLevel >= _difficultyLevel;

            case EqualSign.LESS_OR_EQUAL:
                return currentDifficultyLevel <= _difficultyLevel;

            default:
                return false;
        }
    }
}

﻿using UnityEditor;

using Arbor;

namespace ArborEditor
{
	[CustomEditor(typeof(ArborFSM))]
	public class ArborFSMInspector : ArborFSMInternalInspector
	{
	}
}
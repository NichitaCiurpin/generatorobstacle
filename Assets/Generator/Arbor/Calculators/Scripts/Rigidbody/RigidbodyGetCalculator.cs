﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// GameObjectにアタッチされているRigidbodyを取得する。
	/// </summary>
#else
	/// <summary>
	/// Gets the Rigidbody attached to GameObject.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody/Rigidbody.Get")]
	[BuiltInCalculator]
	public class RigidbodyGetCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// GameObject
		/// </summary>
		[SerializeField] private FlexibleGameObject _GameObject;

#if ARBOR_DOC_JA
		/// <summary>
		/// 取得したRigidbody
		/// </summary>
#else
		/// <summary>
		/// Get the Rigidbody
		/// </summary>
#endif
		[SerializeField] private OutputSlotRigidbody _Rigidbody;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			GameObject gameObject = _GameObject.value;
			if (gameObject != null)
			{
				_Rigidbody.SetValue(gameObject.GetComponent<Rigidbody>());
            }
		}
	}
}

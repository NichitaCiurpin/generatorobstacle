﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// オブジェクトの回転に対する抵抗
	/// </summary>
#else
	/// <summary>
	/// The angular drag of the object.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody/Rigidbody.AngularDrag")]
	[BuiltInCalculator]
	public class RigidbodyAngularDragCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rigidbody
		/// </summary>
		[SerializeField] private FlexibleRigidbody _Rigidbody;

#if ARBOR_DOC_JA
		/// <summary>
		/// オブジェクトの回転に対する抵抗
		/// </summary>
#else
		/// <summary>
		/// The angular drag of the object.
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _AngularDrag;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Rigidbody rigidbody = _Rigidbody.value;
			if (rigidbody != null)
			{
				_AngularDrag.SetValue(rigidbody.angularDrag);
            }
		}
	}
}

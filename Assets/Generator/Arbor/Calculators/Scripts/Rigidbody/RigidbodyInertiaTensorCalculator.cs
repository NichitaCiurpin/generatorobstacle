﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 質量の中心に対する、相対的な対角慣性テンソル
	/// </summary>
#else
	/// <summary>
	/// The diagonal inertia tensor of mass relative to the center of mass.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody/Rigidbody.InertiaTensor")]
	[BuiltInCalculator]
	public class RigidbodyInertiaTensorCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rigidbody
		/// </summary>
		[SerializeField] private FlexibleRigidbody _Rigidbody;

#if ARBOR_DOC_JA
		/// <summary>
		/// 相対的な対角慣性テンソル
		/// </summary>
#else
		/// <summary>
		/// The diagonal inertia tensor of mass relative
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _InertiaTensor;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Rigidbody rigidbody = _Rigidbody.value;
			if (rigidbody != null)
			{
				_InertiaTensor.SetValue(rigidbody.inertiaTensor);
            }
		}
	}
}

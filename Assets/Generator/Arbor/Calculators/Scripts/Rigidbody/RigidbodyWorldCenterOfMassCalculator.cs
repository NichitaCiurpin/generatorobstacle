﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ワールド座標による質量の中心値
	/// </summary>
#else
	/// <summary>
	/// The center of mass of the rigidbody in world space.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody/Rigidbody.WorldCenterOfMass")]
	[BuiltInCalculator]
	public class RigidbodyWorldCenterOfMassCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rigidbody
		/// </summary>
		[SerializeField] private FlexibleRigidbody _Rigidbody;

#if ARBOR_DOC_JA
		/// <summary>
		/// 質量の中心値
		/// </summary>
#else
		/// <summary>
		/// The center of mass.
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _WorldCenterOfMass;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Rigidbody rigidbody = _Rigidbody.value;
			if (rigidbody != null)
			{
				_WorldCenterOfMass.SetValue(rigidbody.worldCenterOfMass);
            }
		}
	}
}

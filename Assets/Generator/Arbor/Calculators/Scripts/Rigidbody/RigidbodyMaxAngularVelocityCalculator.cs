﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody の最大角速度。
	/// </summary>
#else
	/// <summary>
	/// The maximimum angular velocity of the rigidbody.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody/Rigidbody.MaxAngularVelocity")]
	[BuiltInCalculator]
	public class RigidbodyMaxAngularVelocityCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rigidbody
		/// </summary>
		[SerializeField] private FlexibleRigidbody _Rigidbody;

#if ARBOR_DOC_JA
		/// <summary>
		/// 最大角速度。
		/// </summary>
#else
		/// <summary>
		/// The maximimum angular velocity.
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _MaxAngularVelocity;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Rigidbody rigidbody = _Rigidbody.value;
			if (rigidbody != null)
			{
				_MaxAngularVelocity.SetValue(rigidbody.maxAngularVelocity);
            }
		}
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ワールド座標における、Rigidbody オブジェクトの速度を取得します
	/// </summary>
#else
	/// <summary>
	/// The velocity of the rigidbody at the point worldPoint in global space.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody/Rigidbody.GetPointVelocity")]
	[BuiltInCalculator]
	public class RigidbodyGetPointVelocityCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rigidbody
		/// </summary>
		[SerializeField] private FlexibleRigidbody _Rigidbody;

#if ARBOR_DOC_JA
		/// <summary>
		/// ワールド座標
		/// </summary>
#else
		/// <summary>
		/// the point worldPoint in global space.
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _WorldPoint;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Rigidbody rigidbody = _Rigidbody.value;
			if (rigidbody != null)
			{
				_Result.SetValue( rigidbody.GetPointVelocity(_WorldPoint.value) );
            }
		}
	}
}

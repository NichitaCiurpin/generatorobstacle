﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 当たったCollider
	/// </summary>
#else
	/// <summary>
	/// The Collider that was hit.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("RaycastHit/RaycastHit.Collider")]
	[BuiltInCalculator]
	public class RaycastHitColliderCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// RaycastHit
		/// </summary>
		[SerializeField] private InputSlotRaycastHit _RaycastHit;

#if ARBOR_DOC_JA
		/// <summary>
		/// 当たったColliderを出力
		/// </summary>
#else
		/// <summary>
		/// Output the hit Collider
		/// </summary>
#endif
		[SerializeField] private OutputSlotCollider _Collider;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			RaycastHit raycastHit = new RaycastHit();
			if (_RaycastHit.GetValue(ref raycastHit))
			{
				_Collider.SetValue(raycastHit.collider);
			}
		}
	}
}

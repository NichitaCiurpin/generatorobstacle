﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 衝突したColliderまたはRigidbodyのTransform
	/// </summary>
#else
	/// <summary>
	/// The Transform of the rigidbody or collider that was hit.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("RaycastHit/RaycastHit.Transform")]
	[BuiltInCalculator]
	public class RaycastHitTransformCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// RaycastHit
		/// </summary>
		[SerializeField] private InputSlotRaycastHit _RaycastHit;

#if ARBOR_DOC_JA
		/// <summary>
		/// 当たったTransformを出力
		/// </summary>
#else
		/// <summary>
		/// Output the hitting Transform
		/// </summary>
#endif
		[SerializeField] private OutputSlotTransform _Transform;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			RaycastHit raycastHit = new RaycastHit();
			if (_RaycastHit.GetValue(ref raycastHit) )
			{
				_Transform.SetValue(raycastHit.transform);
            }
        }
	}
}

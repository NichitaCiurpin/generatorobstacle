﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 衝突したライトマップのUV座標
	/// </summary>
#else
	/// <summary>
	/// The uv lightmap coordinate at the impact point.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("RaycastHit/RaycastHit.LightmapCoord")]
	[BuiltInCalculator]
	public class RaycastHitLightmapCoordCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// RaycastHit
		/// </summary>
		[SerializeField] private InputSlotRaycastHit _RaycastHit;

#if ARBOR_DOC_JA
		/// <summary>
		/// ライトマップ座標を出力
		/// </summary>
#else
		/// <summary>
		/// Output the lightmap coordinate
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _LightmapCoord;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			RaycastHit raycastHit = new RaycastHit();
			if (_RaycastHit.GetValue(ref raycastHit) )
			{
				_LightmapCoord.SetValue(raycastHit.lightmapCoord);
            }
        }
	}
}

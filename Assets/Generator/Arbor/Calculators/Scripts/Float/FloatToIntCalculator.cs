﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// floatをintに変換する。
	/// </summary>
#else
	/// <summary>
	/// Convert float to int.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Float/Float.ToInt")]
	[BuiltInCalculator]
	public class FloatToIntCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] FlexibleFloat _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] OutputSlotInt _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue((int)_Value.value);
		}
	}
}

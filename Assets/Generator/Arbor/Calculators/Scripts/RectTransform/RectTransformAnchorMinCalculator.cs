﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 左下の角をアンカーした親 RectTransform で正規化された位置
	/// </summary>
#else
	/// <summary>
	/// The normalized position in the parent RectTransform that the lower left corner is anchored to.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("RectTransform/RectTransform.AnchorMin")]
	[BuiltInCalculator]
	public class RectTransformAnchorMinCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// RectTransform
		/// </summary>
		[SerializeField] private FlexibleRectTransform _RectTransform;

#if ARBOR_DOC_JA
		/// <summary>
		/// アンカーの最小の位置
		/// </summary>
#else
		/// <summary>
		/// Minumum position of anchor
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _AnchorMin;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			RectTransform rectTransform = _RectTransform.value;
			if (rectTransform != null)
			{
				_AnchorMin.SetValue(rectTransform.anchorMin);
			}
		}
	}
}

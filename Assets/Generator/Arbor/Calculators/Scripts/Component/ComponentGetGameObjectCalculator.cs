﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ComponentのGameObjectを出力する。
	/// </summary>
#else
	/// <summary>
	/// Output Component's GameObject.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Component/Component.GetGameObject")]
	[BuiltInCalculator]
	public class ComponentGetGameObjectCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Component
		/// </summary>
		[SerializeField] private InputSlotComponent _Component;

		/// <summary>
		/// GameObject
		/// </summary>
		[SerializeField] private OutputSlotGameObject _GameObject;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			GameObject gameObject = null;
			Component component = null;
			if (_Component.GetValue(ref component) && component != null)
			{
				gameObject = component.gameObject;
			}

			_GameObject.SetValue(gameObject);
		}
	}
}

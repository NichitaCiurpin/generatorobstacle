﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Boundsを中心とサイズに分解する。
	/// </summary>
#else
	/// <summary>
	/// Decompose Bounds to size and size.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Bounds/Bounds.Decompose")]
	[BuiltInCalculator]
	public class BoundsDecomposeCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Bounds
		/// </summary>
		[SerializeField] private FlexibleBounds _Bounds;

#if ARBOR_DOC_JA
		/// <summary>
		/// 中心を出力
		/// </summary>
#else
		/// <summary>
		/// Center output
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Center;

#if ARBOR_DOC_JA
		/// <summary>
		/// サイズを出力
		/// </summary>
#else
		/// <summary>
		/// Size output
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Size;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Bounds bounds = _Bounds.value;

			_Center.SetValue(bounds.center);
			_Size.SetValue(bounds.size);
		}
	}
}

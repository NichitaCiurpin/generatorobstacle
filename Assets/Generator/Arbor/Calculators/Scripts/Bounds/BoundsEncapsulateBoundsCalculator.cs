﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 位置やバウンズを含むように、拡大・縮小する。
	/// </summary>
#else
	/// <summary>
	/// Grow the bounds to encapsulate the bounds.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Bounds/Bounds.EncapsulateBounds")]
	[BuiltInCalculator]
	public class BoundsEncapsulateBoundsCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Bounds 1
		/// </summary>
		[SerializeField] private FlexibleBounds _Bounds1;

		/// <summary>
		/// Bounds 2
		/// </summary>
		[SerializeField] private FlexibleBounds _Bounds2;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBounds _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Bounds bounds = _Bounds1.value;
			bounds.Encapsulate(_Bounds2.value);
			_Result.SetValue(bounds);
		}
	}
}

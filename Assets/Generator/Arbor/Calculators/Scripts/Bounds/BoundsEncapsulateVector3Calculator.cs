﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 設定した位置を含むように拡大する。
	/// </summary>
#else
	/// <summary>
	/// Grows the Bounds to include the point.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Bounds/Bounds.EncapsulateVector3")]
	[BuiltInCalculator]
	public class BoundsEncapsulateVector3Calculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Bounds
		/// </summary>
		[SerializeField] private FlexibleBounds _Bounds;

#if ARBOR_DOC_JA
		/// <summary>
		/// 位置
		/// </summary>
#else
		/// <summary>
		/// Point
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Point;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBounds _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Bounds bounds = _Bounds.value;
			bounds.Encapsulate(_Point.value);
			_Result.SetValue(bounds);
		}
	}
}

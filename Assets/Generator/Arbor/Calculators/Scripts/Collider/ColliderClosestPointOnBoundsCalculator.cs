﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 設定した位置から一番近い、Bounds オブジェクトの最近接点を求める。
	/// </summary>
#else
	/// <summary>
	/// The closest point to the bounding box of the attached collider.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Collider/Collider.ClosestPointOnBounds")]
	[BuiltInCalculator]
	public class ColliderClosestPointOnBoundsCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Collider
		/// </summary>
		[SerializeField] private InputSlotCollider _Collider;

#if ARBOR_DOC_JA
		/// <summary>
		/// 位置
		/// </summary>
#else
		/// <summary>
		/// Position
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Position;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Collider collider = null;
			_Collider.GetValue(ref collider);
            if ( collider != null )
			{
				_Result.SetValue(collider.ClosestPointOnBounds(_Position.value));
            }
		}
	}
}

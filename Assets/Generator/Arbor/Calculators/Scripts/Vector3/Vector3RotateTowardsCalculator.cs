﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 現在の位置Current からTargetに向けてベクトルを回転する。
	/// </summary>
#else
	/// <summary>
	/// Rotates a vector Current towards Target.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector3/Vector3.RotateTowards")]
	[BuiltInCalculator]
	public class Vector3RotateTowardsCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 現在ベクトル
		/// </summary>
#else
		/// <summary>
		/// Current vector
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Current;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標ベクトル
		/// </summary>
#else
		/// <summary>
		/// Target vector
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 最大のラジアン変化量
		/// </summary>
#else
		/// <summary>
		/// The maximum radian of the amount of change
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _MaxRadiansDelta;

#if ARBOR_DOC_JA
		/// <summary>
		/// 最大の長さ変化量
		/// </summary>
#else
		/// <summary>
		/// The maximum length of the amount of change
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _MaxMagnitudeDelta;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		public override void OnCalculate()
		{
            _Result.SetValue( Vector3.RotateTowards(_Current.value, _Target.value, _MaxRadiansDelta.value, _MaxMagnitudeDelta.value) );
		}
	}
}

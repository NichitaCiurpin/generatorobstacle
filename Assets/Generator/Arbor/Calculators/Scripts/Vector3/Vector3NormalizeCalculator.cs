﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Vector3を正規化したベクトル
	/// </summary>
#else
	/// <summary>
	/// Vector3 normalized vector
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector3/Vector3.Normalize")]
	[BuiltInCalculator]
	public class Vector3NormalizeCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Vector3
		/// </summary>
		[SerializeField] private FlexibleVector3 _Vector3;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		public override void OnCalculate()
		{
			_Result.SetValue(_Vector3.value.normalized);
        }
	}
}

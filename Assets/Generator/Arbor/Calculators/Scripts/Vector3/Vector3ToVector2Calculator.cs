﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Vector3をVector2に変換する。
	/// </summary>
#else
	/// <summary>
	/// Vector3 is converted to Vector2.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector3/Vector3.ToVector2")]
	[BuiltInCalculator]
	public class Vector3ToVector2Calculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Vector3
		/// </summary>
		[SerializeField] private FlexibleVector3 _Vector3;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _Result;

		#endregion // Serialize fields

		public override void OnCalculate()
		{
			_Result.SetValue((Vector2)_Vector3.value);
		}
	}
}

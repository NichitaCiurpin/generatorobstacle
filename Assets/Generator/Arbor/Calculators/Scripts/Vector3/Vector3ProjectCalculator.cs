﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ベクトルを別のベクトルに投影する。
	/// </summary>
#else
	/// <summary>
	/// Calculate so that the vector is normalized and orthogonal to other vectors.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector3/Vector3.Project")]
	[BuiltInCalculator]
	public class Vector3ProjectCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// ベクトル
		/// </summary>
#else
		/// <summary>
		/// Vector
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Vector;

#if ARBOR_DOC_JA
		/// <summary>
		/// 法線ベクトル
		/// </summary>
#else
		/// <summary>
		/// Normal vector
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _OnNormal;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		public override void OnCalculate()
		{
            _Result.SetValue( Vector3.Project(_Vector.value, _OnNormal.value) );
		}
	}
}

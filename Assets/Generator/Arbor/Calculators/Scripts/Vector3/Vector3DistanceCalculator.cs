﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// AとBの間の距離を計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculates the distance between A and B.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector3/Vector3.Distance")]
	[BuiltInCalculator]
	public class Vector3DistanceCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// ベクトルA
		/// </summary>
#else
		/// <summary>
		/// Vector A
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _A;

#if ARBOR_DOC_JA
		/// <summary>
		/// ベクトルB
		/// </summary>
#else
		/// <summary>
		/// Vector B
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _B;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Result;

		#endregion // Serialize fields

		public override void OnCalculate()
		{
            _Result.SetValue( Vector3.Distance(_A.value, _B.value) );
		}
	}
}

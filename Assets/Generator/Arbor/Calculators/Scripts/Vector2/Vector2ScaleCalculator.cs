﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 2 つのベクトルの各成分を乗算する。
	/// </summary>
#else
    /// <summary>
    /// Multiplies two vectors component-wise.
    /// </summary>
#endif
    [AddComponentMenu("")]
	[AddCalculatorMenu("Vector2/Vector2.Scale")]
	[BuiltInCalculator]
	public class Vector2ScaleCalculator : Calculator
	{
        #region Serialize fields

        /// <summary>
        /// Vector2
        /// </summary>
		[SerializeField] private FlexibleVector2 _Vector2;

#if ARBOR_DOC_JA
		/// <summary>
		/// 乗算するベクトル
		/// </summary>
#else
        /// <summary>
        /// Vector to multiply
        /// </summary>
#endif
        [SerializeField] private FlexibleVector2 _Scale;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
        /// <summary>
        /// Output result
        /// </summary>
#endif
        [SerializeField] private OutputSlotVector2 _Result;

        #endregion // Serialize fields

        public override void OnCalculate()
		{
			_Result.SetValue( Vector2.Scale(_Vector2.value, _Scale.value) );
		}
	}
}

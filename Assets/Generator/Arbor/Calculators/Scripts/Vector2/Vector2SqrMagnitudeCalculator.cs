﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ベクトルの 2 乗の長さを計算する。
	/// </summary>
#else
    /// <summary>
    /// Calculate the length of the square of the vector.
    /// </summary>
#endif
    [AddComponentMenu("")]
	[AddCalculatorMenu("Vector2/Vector2.SqrMagnitude")]
	[BuiltInCalculator]
	public class Vector2SqrMagnitudeCalculator : Calculator
	{
        #region Serialize fields

        /// <summary>
        /// Vector2
        /// </summary>
		[SerializeField] private FlexibleVector2 _Vector2;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
        /// <summary>
        /// Output result
        /// </summary>
#endif
        [SerializeField] private OutputSlotFloat _Result;

        #endregion // Serialize fields

        public override void OnCalculate()
		{
			_Result.SetValue(_Vector2.value.sqrMagnitude);
		}
	}
}

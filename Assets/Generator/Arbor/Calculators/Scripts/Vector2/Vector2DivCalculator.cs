﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Vector2をfloatで除算する。
	/// </summary>
#else
	/// <summary>
	/// Divide Vector 2 by float.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector2/Vector2.Div")]
	[BuiltInCalculator]
	public class Vector2DivCalculator : Calculator
	{
        #region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値1
		/// </summary>
#else
        /// <summary>
        /// Value 1
        /// </summary>
#endif
        [SerializeField] private FlexibleVector2 _Value1;

#if ARBOR_DOC_JA
		/// <summary>
		/// 値2
		/// </summary>
#else
		/// <summary>
		/// Value 2
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _Value2;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _Result;

        #endregion // Serialize fields

        // Use this for calculate
        public override void OnCalculate()
		{
			_Result.SetValue(_Value1.value / _Value2.value);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 2つのベクトルの内積
	/// </summary>
#else
	/// <summary>
	/// Dot Product of two vectors.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector2/Vector2.Dot")]
	[BuiltInCalculator]
	public class Vector2DotCalculator : Calculator
	{
        #region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 左側の値
		/// </summary>
#else
        /// <summary>
        /// Left side value
        /// </summary>
#endif
        [SerializeField] private FlexibleVector2 _Lhs;

#if ARBOR_DOC_JA
		/// <summary>
		/// 右側の値
		/// </summary>
#else
		/// <summary>
		/// Right side value
		/// </summary>
#endif
		[SerializeField] private FlexibleVector2 _Rhs;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Result;

        #endregion // Serialize fields

        public override void OnCalculate()
		{
			_Result.SetValue(Vector2.Dot(_Lhs.value, _Rhs.value));
		}
	}
}

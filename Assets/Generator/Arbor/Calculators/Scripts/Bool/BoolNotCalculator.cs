﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Bool値をNot演算する。
	/// </summary>
#else
	/// <summary>
	/// Calculate the Bool value by Not.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Bool/Bool.Not")]
	[BuiltInCalculator]
	public class BoolNotCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] private FlexibleBool _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBool _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(!_Value.value);
		}
	}
}

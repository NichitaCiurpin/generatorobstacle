﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// intの符号を反転する。
	/// </summary>
#else
	/// <summary>
	/// To invert the sign of the int.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Int/Int.Negative")]
	[BuiltInCalculator]
	public class IntNegativeCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] private FlexibleInt _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotInt _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(-_Value.value);
        }
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Quaternion として保存されるワールド空間での Transform の回転
	/// </summary>
#else
	/// <summary>
	/// The rotation of the transform in world space stored as a Quaternion.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Transform/Transform.Rotation")]
	[BuiltInCalculator]
	public class TransformRotationCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Transform
		/// </summary>
		[SerializeField] private FlexibleTransform _Transform;

#if ARBOR_DOC_JA
		/// <summary>
		/// ワールド空間での Transform の回転
		/// </summary>
#else
		/// <summary>
		/// The rotation of the transform in world space.
		/// </summary>
#endif
		[SerializeField] private OutputSlotQuaternion _Rotation;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Transform transform = _Transform.value;
			if (transform != null)
			{
				_Rotation.SetValue(transform.rotation);
            }
		}
	}
}

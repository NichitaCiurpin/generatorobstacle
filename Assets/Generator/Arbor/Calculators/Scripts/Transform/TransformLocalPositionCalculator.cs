﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 親の Transform オブジェクトから見た相対的な位置
	/// </summary>
#else
	/// <summary>
	/// Position of the transform relative to the parent transform.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Transform/Transform.LocalPosition")]
	[BuiltInCalculator]
	public class TransformLocalPositionCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Transform
		/// </summary>
		[SerializeField] private FlexibleTransform _Transform;

#if ARBOR_DOC_JA
		/// <summary>
		/// 相対的な位置
		/// </summary>
#else
		/// <summary>
		/// Position of the transform relative.
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _LocalPosition;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Transform transform = _Transform.value;
			if (transform != null)
			{
				_LocalPosition.SetValue(transform.localPosition);
            }
		}
	}
}

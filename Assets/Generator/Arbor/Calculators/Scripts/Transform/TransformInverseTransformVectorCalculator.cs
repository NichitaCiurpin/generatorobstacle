﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ワールド空間からローカル空間へ Vector を変換する。
	/// </summary>
#else
	/// <summary>
	/// Transforms a Vector from world space to local space.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Transform/Transform.InverseTransformVector")]
	[BuiltInCalculator]
	public class TransformInverseTransformVectorCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Transform
		/// </summary>
		[SerializeField] private FlexibleTransform _Transform;

#if ARBOR_DOC_JA
		/// <summary>
		/// ワールド空間でのベクトル
		/// </summary>
#else
		/// <summary>
		/// Vector in world space
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Vector;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;
		
		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Transform transform = _Transform.value;
			if (transform != null)
			{
				_Result.SetValue(transform.InverseTransformVector(_Vector.value));
            }
		}
	}
}

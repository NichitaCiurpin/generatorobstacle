﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ワールド空間からローカル空間へ Direction を変換する。
	/// </summary>
#else
	/// <summary>
	/// Transforms a direction from world space to local space.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Transform/Transform.InverseTransformDirection")]
	[BuiltInCalculator]
	public class TransformInverseTransformDirectionCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Transform
		/// </summary>
		[SerializeField] private FlexibleTransform _Transform;

#if ARBOR_DOC_JA
		/// <summary>
		/// ワールド空間での方向
		/// </summary>
#else
		/// <summary>
		/// Direction in world space
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Direction;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Transform transform = _Transform.value;
			if (transform != null)
			{
				_Result.SetValue(transform.InverseTransformDirection(_Direction.value));
            }
		}
	}
}

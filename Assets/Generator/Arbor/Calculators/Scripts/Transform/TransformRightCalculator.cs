﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ワールド空間の Transform の赤軸
	/// </summary>
#else
	/// <summary>
	/// The red axis of the transform in world space.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Transform/Transform.Right")]
	[BuiltInCalculator]
	public class TransformRightCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Transform
		/// </summary>
		[SerializeField] private FlexibleTransform _Transform;

#if ARBOR_DOC_JA
		/// <summary>
		/// ワールド空間の Transform の赤軸
		/// </summary>
#else
		/// <summary>
		/// The red axis of the transform in world space.
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Right;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Transform transform = _Transform.value;
			if (transform != null)
			{
				_Right.SetValue(transform.right);
            }
		}
	}
}

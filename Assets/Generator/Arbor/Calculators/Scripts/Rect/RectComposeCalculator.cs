﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rectを作成する。
	/// </summary>
#else
    /// <summary>
    /// Compose Rect.
    /// </summary>
#endif
    [AddComponentMenu("")]
	[AddCalculatorMenu("Rect/Rect.Compose")]
	[BuiltInCalculator]
	public class RectComposeCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 矩形を作成する基準点の X 値
		/// </summary>
#else
		/// <summary>
		/// The X value the rect is measured from.
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _X;

#if ARBOR_DOC_JA
		/// <summary>
		/// 矩形を作成する基準点の Y 値
		/// </summary>
#else
        /// <summary>
        /// The Y value the rect is measured from.
        /// </summary>
#endif
        [SerializeField] private FlexibleFloat _Y;

#if ARBOR_DOC_JA
		/// <summary>
		/// 矩形の幅
		/// </summary>
#else
        /// <summary>
        /// The width of the rectangle.
        /// </summary>
#endif
        [SerializeField] private FlexibleFloat _Width;

#if ARBOR_DOC_JA
		/// <summary>
		/// 矩形の高さ
		/// </summary>
#else
        /// <summary>
        /// 	The height of the rectangle.
        /// </summary>
#endif
        [SerializeField] private FlexibleFloat _Height;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
        /// <summary>
        /// Result output
        /// </summary>
#endif
        [SerializeField] private OutputSlotRect _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(new Rect(_X.value, _Y.value, _Width.value, _Height.value));
        }
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// PointがRectの内側にあるかどうか。
	/// </summary>
#else
	/// <summary>
	/// Whether the Point is inside the Rect.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rect/Rect.Contains")]
	[BuiltInCalculator]
	public class RectContainsCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rect
		/// </summary>
		[SerializeField] private FlexibleRect _Rect;

#if ARBOR_DOC_JA
		/// <summary>
		/// 判定する点。
		/// </summary>
#else
		/// <summary>
		/// Point to test.
		/// </summary>
#endif
		[SerializeField] private FlexibleVector2 _Point;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBool _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(_Rect.value.Contains(_Point.value));
        }
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Value以上のもっとも近い2のべき乗を計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculates the next power of two value.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Mathf/Mathf.NextPowerOfTwo")]
	[BuiltInCalculator]
	public class MathfNextPowerOfTwoCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] private FlexibleInt _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotInt _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Mathf.NextPowerOfTwo(_Value.value));
		}
	}
}

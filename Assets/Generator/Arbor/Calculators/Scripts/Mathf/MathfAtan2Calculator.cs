﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// atan2を計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculate atan2.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Mathf/Mathf.Atan2")]
	[BuiltInCalculator]
	public class MathfAtan2Calculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値Y
		/// </summary>
#else
		/// <summary>
		/// Value Y
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _Y;

#if ARBOR_DOC_JA
		/// <summary>
		/// 値X
		/// </summary>
#else
		/// <summary>
		/// Value X
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _X;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Mathf.Atan2(_Y.value,_X.value));
		}
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 2つのint値から最大値を計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculate the maximum value from two int values.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Mathf/Mathf.MaxInt")]
	[BuiltInCalculator]
	public class MathfMaxIntCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値A
		/// </summary>
#else
		/// <summary>
		/// Value A
		/// </summary>
#endif
		[SerializeField] private FlexibleInt _A;

#if ARBOR_DOC_JA
		/// <summary>
		/// 値B
		/// </summary>
#else
		/// <summary>
		/// Value B
		/// </summary>
#endif
		[SerializeField] private FlexibleInt _B;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotInt _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Mathf.Max(_A.value, _B.value));
		}
	}
}

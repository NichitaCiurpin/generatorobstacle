﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 値以上の最小の整数を計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculate the smallest integer greater than or equal to the value.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Mathf/Mathf.Ceil")]
	[BuiltInCalculator]
	public class MathfCeilCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Mathf.Ceil(_Value.value));
		}
	}
}

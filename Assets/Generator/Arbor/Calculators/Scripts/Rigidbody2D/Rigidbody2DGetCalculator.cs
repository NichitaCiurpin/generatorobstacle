﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// GameObjectにアタッチされているRigidbody2Dを取得する。
	/// </summary>
#else
	/// <summary>
	/// Gets the Rigidbody2D attached to GameObject.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody2D/Rigidbody2D.Get")]
	[BuiltInCalculator]
	public class Rigidbody2DGetCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// GameObject
		/// </summary>
		[SerializeField] private FlexibleGameObject _GameObject;

#if ARBOR_DOC_JA
		/// <summary>
		/// 取得したRigidbody2D
		/// </summary>
#else
		/// <summary>
		/// Get the Rigidbody2D
		/// </summary>
#endif
		[SerializeField] private OutputSlotRigidbody2D _Rigidbody2D;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			GameObject gameObject = _GameObject.value;
			if (gameObject != null)
			{
				_Rigidbody2D.SetValue(gameObject.GetComponent<Rigidbody2D>());
            }
		}
	}
}

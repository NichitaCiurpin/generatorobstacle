﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Quaternionからオイラー角の値を計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculate the Euler angle value from Quaternion.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Quaternion/Quaternion.EulerAngles")]
	[BuiltInCalculator]
	public class QuaternionEulerAnglesCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 入力値
		/// </summary>
#else
		/// <summary>
		/// Input value
		/// </summary>
#endif
		[SerializeField] private FlexibleQuaternion _Input;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(_Input.value.eulerAngles);
		}
	}
}

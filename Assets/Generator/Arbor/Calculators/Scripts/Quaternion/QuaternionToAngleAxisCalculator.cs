﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Quaternionを回転軸と角度に変換する。
	/// </summary>
#else
	/// <summary>
	/// Convert Quaternion to rotation axis and angle.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Quaternion/Quaternion.ToAngleAxis")]
	[BuiltInCalculator]
	public class QuaternionToAngleAxisCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Quaternion
		/// </summary>
		[SerializeField] private FlexibleQuaternion _Quaternion;

#if ARBOR_DOC_JA
		/// <summary>
		/// 角度を出力
		/// </summary>
#else
		/// <summary>
		/// Output angle
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Angle;

#if ARBOR_DOC_JA
		/// <summary>
		/// 回転軸を出力
		/// </summary>
#else
		/// <summary>
		/// Output rotation axis
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Axis;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			float angle;
			Vector3 axis;

			_Quaternion.value.ToAngleAxis(out angle, out axis);

			_Angle.SetValue(angle);
			_Axis.SetValue(axis);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Vector3にQuaternionを乗算する。
	/// </summary>
#else
    /// <summary>
    /// Multiply Vector3 by Quaternion.
    /// </summary>
#endif
    [AddComponentMenu("")]
	[AddCalculatorMenu("Quaternion/Quaternion.MulVector3")]
	[BuiltInCalculator]
	public class QuaternionMulVector3Calculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Quaternion
		/// </summary>
		[SerializeField]
		private FlexibleQuaternion _Quaternion;

        /// <summary>
        /// Vector3
        /// </summary>
		[SerializeField]
		private FlexibleVector3 _Vector3;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
        /// <summary>
        /// Result output
        /// </summary>
#endif
        [SerializeField]
		private OutputSlotVector3 _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(_Quaternion.value * _Vector3.value);
        }
	}
}

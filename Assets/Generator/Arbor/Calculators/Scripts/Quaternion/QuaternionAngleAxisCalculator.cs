﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Axisの周りをAngle度回転するQuaternionを作成する。
	/// </summary>
#else
	/// <summary>
	/// Create a Quaternion that rotates Angle degrees around Axis.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Quaternion/Quaternion.AngleAxis")]
	[BuiltInCalculator]
	public class QuaternionAngleAxisCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 角度
		/// </summary>
#else
		/// <summary>
		/// Angle
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _Angle;

#if ARBOR_DOC_JA
		/// <summary>
		/// 回転軸
		/// </summary>
#else
		/// <summary>
		/// Axis of rotation
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Axis;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotQuaternion _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Quaternion.AngleAxis(_Angle.value, _Axis.value));
		}
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 逆Quaternionを計算する。
	/// </summary>
#else
    /// <summary>
    /// Calculates the reverse Quaternion.
    /// </summary>
#endif
    [AddComponentMenu("")]
	[AddCalculatorMenu("Quaternion/Quaternion.Inverse")]
	[BuiltInCalculator]
	public class QuaternionInverseCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 入力値
		/// </summary>
#else
		/// <summary>
		/// Input value
		/// </summary>
#endif
		[SerializeField] private FlexibleQuaternion _Input;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
        /// <summary>
        /// Result output
        /// </summary>
#endif
        [SerializeField] private OutputSlotQuaternion _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Quaternion.Inverse(_Input.value));
        }
	}
}

﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// CollisionからヒットしたColliderを出力する。
	/// </summary>
#else
	/// <summary>
	/// Output Collider hit from Collision.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Collision/Collision.Collider")]
	[BuiltInCalculator]
	public class CollisionColliderCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Collision
		/// </summary>
		[SerializeField] private InputSlotCollision _Collision;

#if ARBOR_DOC_JA
		/// <summary>
		/// ヒットしたCollider。
		/// </summary>
#else
		/// <summary>
		/// Collider hit.
		/// </summary>
#endif
		[SerializeField] private OutputSlotCollider _Collider;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Collision collision = null;
			_Collision.GetValue(ref collision);
            if (collision != null )
			{
				_Collider.SetValue(collision.collider);
            }
		}
	}
}

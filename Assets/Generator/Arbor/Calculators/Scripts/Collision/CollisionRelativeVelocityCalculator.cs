﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 衝突した2つのオブジェクトの相対速度を出力する。
	/// </summary>
#else
	/// <summary>
	/// Output the relative velocity of the two collided objects.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Collision/Collision.RelativeVelocity")]
	[BuiltInCalculator]
	public class CollisionRelativeVelocityCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Collision
		/// </summary>
		[SerializeField] private InputSlotCollision _Collision;

#if ARBOR_DOC_JA
		/// <summary>
		/// 相対速度。
		/// </summary>
#else
		/// <summary>
		/// Relative velocity.
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _RelativeVelocity;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Collision collision = null;
			_Collision.GetValue(ref collision);
            if (collision != null )
			{
				_RelativeVelocity.SetValue(collision.relativeVelocity);
            }
		}
	}
}

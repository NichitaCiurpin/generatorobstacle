﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// CollisionからヒットしたTransformを出力する。
	/// </summary>
#else
	/// <summary>
	/// Output Transform hit from Collision.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Collision/Collision.Transform")]
	[BuiltInCalculator]
	public class CollisionTransformCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Collision
		/// </summary>
		[SerializeField] private InputSlotCollision _Collision;

#if ARBOR_DOC_JA
		/// <summary>
		/// ヒットしたTransform。
		/// </summary>
#else
		/// <summary>
		/// Transform hit.
		/// </summary>
#endif
		[SerializeField] private OutputSlotTransform _Transform;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Collision collision = null;
			_Collision.GetValue(ref collision);
            if (collision != null )
			{
				_Transform.SetValue(collision.transform);
            }
		}
	}
}

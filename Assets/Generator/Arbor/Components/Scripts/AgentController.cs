using UnityEngine;
#if UNITY_5_5_OR_NEWER
using UnityEngine.AI;
#endif
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// NavMeshAgentをラップしたAI用移動コンポーネント。<br />
	/// 主に組み込みBehaviourのAgentを介して使用する。
	/// </summary>
#else
	/// <summary>
	/// AI for the movement component that wraps the NavMeshAgent.<br />
	/// Used mainly through built-in Behavior's Agent.
	/// </summary>
#endif
	[AddComponentMenu("Arbor/AgentController",40)]
	[BuiltInComponent]
	[HelpURL( "http://arbor-docs.caitsithware.com/components/agentcontroller.html" )]
	public class AgentController : MonoBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 制御したいNavMeshAgent。
		/// </summary>
#else
		/// <summary>
		/// NavMeshAgent you want to control.
		/// </summary>
#endif
		[SerializeField] private NavMeshAgent _Agent;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動速度をAnimatorへ渡すためのSpeedパラメータを指定する。
		/// </summary>
#else
		/// <summary>
		/// Specify the Speed parameter for passing the moving speed to the Animator.
		/// </summary>
#endif
		[SerializeField] private AnimatorParameterReference _SpeedParameter;

		#endregion // Serialize fields

		private Vector3 _StartPosition;

		// Use this for initialization
		void Start()
		{
			if (_Agent == null)
			{
				_Agent = GetComponent<NavMeshAgent>();
			}
			
			_StartPosition = transform.position;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始位置から指定半径内をうろつく
		/// </summary>
		/// <param name="speed">移動速度</param>
		/// <param name="radius">開始位置からの半径</param>
#else
		/// <summary>
		/// Prowl the within a specified radius from the start position
		/// </summary>
		/// <param name="speed">Movement speed</param>
		/// <param name="radius">Radius from the starting position</param>
#endif
		public void Patrol(float speed, float radius)
		{
			float angle = Random.Range(-180.0f, 180.0f);

			Vector3 axis = Vector3.up;

			Quaternion rotate = Quaternion.AngleAxis(angle, axis);

			Vector3 dir = rotate * Vector3.forward * Random.Range(0.0f, radius);

			_Agent.speed = speed;
			_Agent.stoppingDistance = 0.0f;
			_Agent.SetDestination(_StartPosition + dir);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 指定したTransformの位置へ近づく
		/// </summary>
		/// <param name="speed">移動速度</param>
		/// <param name="stoppingDistance">停止距離</param>
		/// <param name="target">目標地点</param>
#else
		/// <summary>
		/// Approach to the position of the specified Transform
		/// </summary>
		/// <param name="speed">Movement speed</param>
		/// <param name="stoppingDistance">Stopping distance</param>
		/// <param name="target">Objective point</param>
#endif
		public void Follow(float speed, float stoppingDistance, Transform target)
		{
			if( target == null )
			{
				return;
			}
			_Agent.speed = speed;
			_Agent.stoppingDistance = stoppingDistance;
			_Agent.SetDestination(target.position);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 指定したTransformから遠ざかる
		/// </summary>
		/// <param name="speed">移動速度</param>
		/// <param name="distance">遠ざかる距離</param>
		/// <param name="target">対象</param>
#else
		/// <summary>
		/// Away from the specified Transform
		/// </summary>
		/// <param name="speed">Movement speed</param>
		/// <param name="distance">Distance away</param>
		/// <param name="target">Target</param>
#endif
		public void Escape(float speed, float distance, Transform target)
		{
			if( target == null )
			{
				return;
			}

			Vector3 dir = transform.position - target.position;

			if (dir.magnitude >= distance)
			{
				return;
			}

			Vector3 pos = dir.normalized * distance + target.position;

			_Agent.speed = speed;
			_Agent.stoppingDistance = 0.0f;
			if (!_Agent.SetDestination(pos))
			{
				pos = -dir.normalized * distance + transform.position;

				_Agent.SetDestination(pos);
			}
		}

		void Update()
		{
			if (_SpeedParameter.animator != null)
			{
				_SpeedParameter.animator.SetFloat(_SpeedParameter.name, _Agent.velocity.magnitude);
			}
		}
	}
}

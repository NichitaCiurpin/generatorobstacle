-----------------------------------------------------
            Arbor: State Diagram Editor

          Copyright (c) 2014 Cait Sith Ware
          http://caitsithware.com/wordpress/
          support@caitsithware.com
-----------------------------------------------------

Arbor2 Trial版をダウンロードしていただきありがとうございます！

【Trial版の制限】

1. DLL

* コアの処理はDLL化しているため編集できません。

2. 実行時の制限

* ゲーム開始後2分経過で状態遷移が無効化。
* ゲーム画面に「Arbor2 Trial」の表示。

(Unityエディタ上では制限なくご利用いただけます)

【主な流れ】

1. GameObjectにArborFSMをアタッチ。
2. ArborFSMのインスペクタにあるOpen Editorボタンをクリック
3. Arbor EditorでStateを作成。
4. StateにBehaviourをアタッチ。
5. BehaviourからStateへの遷移を接続。

【サンプルシーン】

サンプルシーンは以下にあります。
Assets/Arbor/Examples/Scenes/

【ドキュメント】

詳しいドキュメントはこちらをご覧ください。
http://arbor.caitsithware.com/

【アセットストア】

ご購入はこちら！
https://www.assetstore.unity3d.com/#!/content/47081

【サポート】

フォーラム : http://forum-arbor.caitsithware.com/

メール : support@caitsithware.com

【更新履歴】

Ver 2.1.1:

* Arbor Editor
- Change : ブレークポイントとステートカウントの表示を枠外に移動
- Fix : ParameterContainerのStringでコピー&ペーストできなかったのを修正。
- Fix : データスロットがスクリプトから削除された場合にCalculatorBranchも削除するように修正

Ver 2.1.0:

* Arbor Editor
- Add : GameObjectを選択しても切り替わらないようにロックするトグル追加。
- Add : StateBehaviourのタイトルバーをドラッグして並び替えできるように対応。
- Add : StateBehaviourをドラッグ&ドロップで任意の位置に挿入できるように対応。
- Add : Stateにブレークポイントを設定できるように対応。
- Add : 実行中にStateとStateLinkが通った回数を表示するように対応。
- Add : 実行中に直前に通ったStateLinkを強調表示するように対応。
- Add : 実行中にCalculaterBranchの値を表示するように対応。
- Add : 組み込みコンポーネントのヘルプボタンからヘルプページを開く。
- Add : 組み込みCalculatorのヘルプボタンからヘルプページを開く。
- Add : CalculatorBranchの型によって線の色を変更。
- Fix : ArborFSMを別のGameObjectに移動したときに入出力スロットからデータにアクセスできなくなっていたのを修正。
- Fix : Arbor Editorのグラフ表示エリアがずれるのを修正。
- Fix : ステートリストから選択する場合など、選択したステートまで自動的にスクロールした時にArbor Editorのグラフ表示が滲むのを修正。
- Add : ArborEditorウィンドウにアイコン追加

* 組み込み挙動
- Change : FindGameObject、FindWithTagGameObjectで見つけたGameObjectを演算ノードへ出力するように対応。
- Add : RandomTransition追加。
- Change : TimeTransitionにTimeTypeの指定を追加。
- Fix : Flexibleなコンポーネントの参照でのキャッシュ処理を修正。
- Fix : 配列にStateLinkがあるBehaviourでエディタ上で配列のサイズを減らすとエラーが出るのを修正。

* Script
- Change : OnStateTriggerをStateBehaviourの仮想関数に変更。
- Fix : AgentController.FollowとEscapeにnullが渡されたときにエラーが出ないように修正。

* その他
- Change : リファレンスサイトを更新。
- Change : Unity最低動作バージョンを5.3.0f4に引き上げ。

Ver 2.0.10:

* Arbor Editor
- Change : GameObjectを選択した際、Arbor Editorも連動して表示が切り替わるように対応。
- Fix : コメントを新規作成するとエラーが出るのを修正。
- Fix : ノードのコピーを行うとプレイ開始時にエラーが出るのを修正。
- Fix : ノードをコピーし一度プレイ開始したあとペーストできなくなるのを修正。
- Fix : Calculatorノードのコピーを修正。
- Fix : CalculatorSlotを持ったStateBehaviourやCalculatorをコピー&ペーストや複製した時の処理を修正。

Ver 2.0.9:

* Arbor Editor
- Fix : Unity5.6でArborEditorを開くとエラーが出るのを修正。
- Fix : Arbor Editorウィンドウの高速化。

* 組み込み挙動
- Change : CalculatorTransitionのBoolを２つのBool値を比較するように変更。

* スクリプト
- Change : State.behaviourCountとGetBehaviourFromIndex追加。State.behavioursを非推奨に。
- Change : ArborFSMInternal.stateCountとGetStateFromIndex追加。ArborFSMInternal.statesを非推奨に。
- Change : ArborFSMInternal.commentCountとGetCommentFromIndex追加。ArborFSMInternal.commentsを非推奨に。
- Change : ArborFSMInternal.calculatorCountとGetCalculatorFromIndex追加。ArborFSMInternal.calculatorsを非推奨に。
- Change : ArborFSMInternal.calculatorBranchCountとGetCalculatorBranchFromIndex追加。ArborFSMInternal.calculatorBranchiesを非推奨に。

Ver 2.0.8:

* Arbor Editor
- Fix : Unity5.3.4以降のArbor EditorでNodeやStateBehaviourをコピーするとエラーが表示されるのを修正。
- Change : ArborFSMが先に実行されるようにScript Execution Orderを変更。

Ver 2.0.7:

* Arbor Editor
- Add : OutputSlotStringとInputSlotString追加
- Add : FlexibleString追加
- Add : ParameterContainerにstring追加
- Add : CalcParameterにstringの処理追加
- Add : ParameterTransitionにstringによる遷移追加
- Add : UISetTextFromParameterにstringパラメータからのテキスト設定に対応
- Fix : OutputSlot/InputSlotをカスタマイズしたクラスを作成した際にArbor Editorにスロットが正常に表示されないのを修正

Ver 2.0.6:

* Arbor Editor
- Fix : ノードを削除したときのUndo/Redoを修正。
- Fix : StateBehaviourを削除した時のUndo/Redoを修正。

Ver 2.0.5:

* Arbor Editor
- Fix : 演算ノードやステート挙動を追加した時に、ArborEditorウィンドウを再描画するように修正。
- Fix : RectUtilityをArborEditor名前空間に修正。
- Fix : Boo用テンプレート修正。

* 組み込み挙動
- Change : Tween系のパラメータを演算ノードから受け取れるように変更。

Ver 2.0.4:

* Arbor Editor
- Add : Stateに初めて入った際にOnStateAwake()を呼ぶように追加。
- Fix : 遷移矢印を右クリックすると遷移先に移動できるメニューをMacでのcontrol+クリックでも表示するように修正。
- Fix : Unity5.5.0Betaでの警告とエラー修正。

Ver 2.0.3:
* Arbor Editor
- Fix : Unity5.4.0Betaで警告が出るのを修正
- Fix : ステートのペーストや複製時にマウスの位置にステートが生成されないのを修正

Ver 2.0.2:
* Arbor Editor
- Fix : Unity5.3.0以降のUnityエディタ上でArborFSMオブジェクトを選択したままプレイ開始するとStateBehaviourが削除されてしまうのを修正。

* 組み込み挙動
- Add : Scene/LoadLevelにAdditiveプロパティを設定できるように対応。
- Add : Scene/UnloadLevel追加(Unity5.2以降対応)。
- Fix : Unity5.3.0以降のScene/LoadLevelにてApplication.LoadLevelの警告が出るのを修正。

Ver 2.0.1:
* Arbor Editor
- Change : ヒープメモリの使用量を削減。
- Fix : コンパイルするたびにエディタ管理用オブジェクトが増えていたのを修正。

* 組み込み挙動
- Change : Audio/PlaySoundAtPointをAudio/PlaySoundAtTransformに改名。
- Add : Audio/PlaySoundAtTransformにAudioMixerGroupとSpatialBlendの指定を追加。
- Add : 新たに座標指定のAudio/PlaySoundAtPointを追加。

Ver 2.0.0:
* Arbor Editor
- Add : 演算ノード追加。
- Add : ParameterContainerでVector2を保持できるように対応。
- Add : ParameterContainerでVector3を保持できるように対応。
- Add : ParameterContainerでQuaternionを保持できるように対応。
- Add : ParameterContainerでRectを保持できるように対応。
- Add : ParameterContainerでBoundsを保持できるように対応。
- Add : ParameterContainerでTransformを保持できるように対応。
- Add : ParameterContainerでRectTransformを保持できるように対応。
- Add : ParameterContainerでRigidbodyを保持できるように対応。
- Add : ParameterContainerでRigidbody2Dを保持できるように対応。

* 組み込み挙動
- Add : Transition/Physics/RaycastTransition
- Add : Transition/Physics2D/Raycast2DTransition
- Add : Transition/CalculatorTransition
- Add : InstantiateGameObjectに生成したGameObjectの出力を追加
- Add : OnCollisionEnterTransitionに当たった相手のCollisionの出力を追加
- Add : OnCollisionExitTransitionに当たった相手のCollisionの出力を追加
- Add : OnCollisionStayTransitionに当たった相手のCollisionの出力を追加
- Add : OnTriggerEnterTransitionに当たった相手のColliderの出力を追加
- Add : OnTriggerExitTransitionに当たった相手のColliderの出力を追加
- Add : OnTriggerStayTransitionに当たった相手のColliderの出力を追加
- Add : OnCollisionEnter2DTransitionに当たった相手のCollision2Dの出力を追加
- Add : OnCollisionExit2DTransitionに当たった相手のCollision2Dの出力を追加
- Add : OnCollisionStayT2Dransitionに当たった相手のCollision2Dの出力を追加
- Add : OnTriggerEnter2DTransitionに当たった相手のCollider2Dの出力を追加
- Add : OnTriggerExit2DTransitionに当たった相手のCollider2Dの出力を追加
- Add : OnTriggerStayT2Dransitionに当たった相手のCollider2Dの出力を追加
- Change : AgentEscapeをFlexibleTransformに対応。
- Change : AgentFllowをFlexibleTransformに対応。
- Change : PlaySoundAtPointをFlexibleTransformに対応。
- Change : InstantiateGameObjectをFlexibleTransformに対応。
- Change : LookAtGameObjectをFlexibleTransformに対応。
- Change : AddForceRigidbodyをFlexibleRigidbodyに対応。
- Change : AddVelocityRigidbodyをFlexibleRigidbodyに対応。
- Change : SetVelocityRigidbodyをFlexibleRigidbodyに対応。
- Change : AddForceRigidbody2DをFlexibleRigidbody2Dに対応。
- Change : AddVelocityRigidbody2DをFlexibleRigidbody2Dに対応。
- Change : SetVelocityRigidbody2DをFlexibleRigidbody2Dに対応。

* 組み込み演算
- Add : BoolのCalculator追加
- Add : BoundsのCalculator追加
- Add : ColliderのCalculator追加
- Add : Collider2DのCalculator追加
- Add : CollisionのCalculator追加
- Add : Collision2DのCalculator追加
- Add : ComponentのCalculator追加
- Add : FloatのCalculator追加
- Add : IntのCalculator追加
- Add : MathfのCalculator追加
- Add : QuaternionのCalculator追加
- Add : RaycastHitのCalculator追加
- Add : RaycastHit2DのCalculator追加
- Add : RectのCalculator追加
- Add : RectTransformのCalculator追加
- Add : RigidbodyのCalculator追加
- Add : Rigidbody2DのCalculator追加
- Add : TransformのCalculator追加
- Add : Vector2のCalculator追加
- Add : Vector3のCalculator追加

* スクリプト
- Add : FlexibleBounds実装
- Add : FlexibleQuaternion実装
- Add : FlexibleRect実装
- Add : FlexibleRectTransform実装
- Add : FlexibleRigidbody実装
- Add : FlexibleRigidbody2D実装
- Add : FlexibleTransform実装
- Add : FlexibleVector2実装
- Add : FlexibleVector3実装

Ver 1.7.7p2:
* Arbor Editor
- Fix : Unity5.2.1以降でエラーが出るのを修正。

Ver 1.7.7p1:
* Arbor Editor
- Fix : ステートとコメントの作成と削除がUndoできなかったのを修正。

Ver 1.7.7:
* Arbor Editor
- Add : ParameterContainerでGameObjectを保持できるように対応。
- Change : 自分自身のステートへ遷移できるように変更。
- Change : 挙動の背景を変更。
- Change : ListGUIの背景を変更。
- Change : コメントノードを内容によってリサイズするように変更。
- Fix : Undo周りのバグ修正
- Fix : 常駐ステートが開始ステートに設定できたのを修正。
- Other : グリッドなどの設定をプロジェクトごとではなくUnityのメジャーバージョンごとに保存するように対応。

* 組み込み挙動
- Add : Collision/OnCollisionEnterStore
- Add : Collision/OnCollisionExitStore
- Add : Collision/OnControllerColliderHitStore
- Add : Collision/OnTriggerEnterStore
- Add : Collision/OnTriggerExitStore
- Add : Collision2D/OnCollisionEnter2DStore
- Add : Collision2D/OnCollisionExit2DStore
- Add : Collision2D/OnTriggerEnter2DStore
- Add : Collision2D/OnTriggerExit2DStore
- Add : GameObject/FindGameObject
- Add : GameObject/FindWithTagGameObject
- Add : UITweenPositionに相対指定できるように追加。
- Add : UITweenSizeに相対指定できるように追加。
- Change : BroadcastMessageGameObjectの値をFlexibleIntなどを使用するように対応。
- Change : CalcAnimatorParameterの値をFlexibleIntなどを使用するように対応。
- Change : CalcParameterの値をFlexibleIntなどを使用するように対応。
- Change : ParameterTransitionの値をFlexibleIntなどを使用するように対応。
- Change : SendMessageGameObjectの値をFlexibleIntなどを使用するように対応。
- Change : SendMessageUpwardsGameObjectの値をFlexibleIntなどを使用するように対応。
- Change : AgentEscapeをArborGameObjectに対応。
- Change : AgentFllowをArborGameObjectに対応。
- Change : ActivateGameObjectをFlexibleGameObjectに対応。
- Change : BroadastMessageGameObjectをFlexibleGameObjectに対応。
- Change : DestroyGameObjectをFlexibleGameObjectに対応。
- Change : LookatGameObjectをFlexibleGameObjectに対応。
- Change : SendMessageGameObjectをFlexibleGameObjectに対応。
- Change : SendMessageUpwardsGameObjectをFlexibleGameObjectに対応。
- Change : BroadcastTriggerをFlexibleGameObjectに対応。
- Change : SendTriggerGameObjectをFlexibleGameObjectに対応。
- Change : SendTriggerUpwardsをFlexibleGameObjectに対応。
- Change : InstantiateGameObjectで生成したオブジェクトをパラメータに格納できるように対応。

* スクリプト
- Add : FlexibleInt実装
- Add : FlexibleFloat実装
- Add : FlexibleBool実装
- Add : FlexibleGameObject実装
- Add : ContextMenuを使えるように対応。

* その他
- Change : Parameter関連をCoreフォルダとInternalフォルダに移動。
- Other : コンポーネントにアイコン設定。

Ver 1.7.6:
* Arbor Editor
- Add : StateLinkに名前設定追加。
- Add : StateLinkに即時遷移フラグ追加。
- Fix : 挙動追加での検索文字列が保存できていなかったのを修正。
- Other : 挙動追加を開いた際、検索バーにフォーカスが移るように対応。
- Other : 挙動追加での並び順で、グループが先に来るように調整。

* コンポーネント
- Add : GlobalParameterContainer

* 組み込み挙動
- Add : Audio/PlaySound
- Add : Audio/StopSound
- Add : Collision/OnCollisionEnterDestroy
- Add : Collision/OnCollisionExitDestroy
- Add : Collision/OnControllerColliderHitDestroy
- Add : Collision2D/OnCollisionEnter2DDestroy
- Add : Collision2D/OnCollisionExit2DDestroy
- Add : GameObject/BroadcastMessageGameObject
- Add : GameObject/SendMessageUpwardsGameObject
- Add : Physics/AddForceRigidbody
- Add : Physics/AddVelocityRigidbody
- Add : Physics2D/AddForceRigidbody2D
- Add : Physics2D/AddVelocityRigidbody2D
- Add : Renderer/SetSprite
- Add : Transition/Collision/OnCollisionEnterTransition
- Add : Transition/Collision/OnCollisionExitTransition
- Add : Transition/Collision/OnCollisionStayTransition
- Add : Transition/Collision/OnControllerColliderHitTransition
- Add : Transition/Collision2D/OnCollisionEnter2DTransition
- Add : Transition/Collision2D/OnCollisionExit2DTransition
- Add : Transition/Collision2D/OnCollisionStay2DTransition
- Add : Transition/Input/ButtonTransition
- Add : Transition/Input/KeyTransition
- Add : Transition/Input/MouseButtonTransition
- Add : Transition/ExistsGameObjectTransition
- Add : Trigger/BroadcastTrigger
- Add : Trigger/SendTriggerGameObject
- Add : Trigger/SendTriggerUpwards
- Add : Tween/TweenRigidbody2DPosition
- Add : Tween/TweenRigidbody2DRotation
- Add : Tween/TweenTextureOffset
- Add : UI/UISetSlider
- Add : UI/UISetSliderFromParameter
- Add : UI/UISetToggle
- Add : UI/UISetToggleFromParameter
- Add : TimeTransitionに現在時間をプログレスバーで表示するように追加。
- Add : Tween終了時に遷移できるように追加。
- Add : TweenPositionに相対指定できるように追加。
- Add : TweenRotationに相対指定できるように追加。
- Add : TweenScaleに相対指定できるように追加。
- Add : TweenRigidbodyPositionに相対指定できるように追加。
- Add : TweenRigidbodyRotationに相対指定できるように追加。
- Fix : OnTriggerExit2DDestroyがCollisionにあったのを修正。
- Fix : CalcAnimatorParameterのfloatValueがintになっていたのを修正。
- Fix : CalcParameterのfloatValueがintになっていたのを修正。
- Fix : ParameterTransitionのfloatValueがintになっていたのを修正。
- Other : SetRigidbodyVelocityをSetVelocityRigidbodyに改名。
- Other : SetRigidbody2DVelocityをSetVelocityRigidbody2Dに改名。

* スクリプト
- Add : FixedImmediateTransition属性で即時遷移フラグを変更できないように対応。

* その他
- Add : Example9としてGlobalParameterContainerのサンプル追加。
- Fix : TagsにCoinが追加されていたので修正。

Ver 1.7.5:
* Arbor Editor
- Fix : グリッドが正しく表示されない時があるのを修正。
- Other : ステートリストの横幅をリサイズできるように対応。

* 組み込み挙動
- Add : Collision/OnTriggerEnterDestroy
- Add : Collision/OnTriggerExitDestroy
- Add : Collision2D/OnTriggerEnter2DDestroy
- Add : Collision2D/OnTriggerExit2DDestroy
- Add : GameObject/LookAtGameObject
- Add : Parameter/SetBoolParameterFromUIToggle
- Add : Parameter/SetFloatParameterFromUISlider
- Add : Physics/SetRigidbodyVelocity
- Add : Physics2D/SetRigidbody2DVelocity
- Add : Transition/EventSystems/OnPointerClickTransition
- Add : Transition/EventSystems/OnPointerDownTransition
- Add : Transition/EventSystems/OnPointerEnterTransition
- Add : Transition/EventSystems/OnPointerExitTransition
- Add : Transition/EventSystems/OnPointerUpTransition
- Add : Tween/TweenCanvasGroupAlpha
- Add : Tween/TweenRigidbodyPosition
- Add : Tween/TweenRigidbodyRotation
- Add : UI/UISetImage
- Add : UI/UISetTextFromParameter
- Add : InstantiateGameObjectで生成時の初期Transformを指定できるように追加。
- Fix : CalcParameterでBool型の場合に正しく動作しなかったのを修正。
- Fix : SendEventGameObjectで呼び出す方をわざわざ指定しないように修正。

* スクリプト
- Add : Parameterにvalueプロパティ追加。
- Add : IntParameterReference追加。
- Add : FloatParameterReference追加。
- Add : BoolParameterReference追加。

* その他
- Add : HierarchyのCreateボタンからArborFSM付きGameObjectを作れるように追加。
- Add : HierarchyのCreateボタンからParameterContainer付きGameObjectを作れるように追加。
- Add : HierarchyのCreateボタンからAgentController付きGameObjectを作れるように追加。
- Add : Example7としてコインプッシャーゲーム追加。
- Add : Example8としてEventSystemのサンプル追加。
- Other : フォルダ整理。

Ver 1.7.4:
- Add : Agent系Behaviour追加。
- Add : uGUI系Behaviour追加。
- Add : uGUI系Tween追加。
- Add : SendEventGameObject追加。
- Add : SendMessageGameObjectに値渡し機能追加。
- Fix : AnimatorParameterReferenceの参照先がAnimatorControllerを参照していなかったときにエラーが出るのを修正。
- Other : uGUI対応に伴いUnity最低動作バージョンを4.6.7f1に引き上げ。

Ver 1.7.3:
- Add : OnMouse系Transition追加
- Fix : 選択ステートへの移動時のスクロール位置修正
- Other : ステートリストを名前順でソートするように変更。
- Other : Arbor Editorの左上方向へも無限にステートを配置できるように変更。
- Other : マニュアルサイトを一新。

Ver 1.7.2:
- Add : ArborEditorにコメントノードを追加。
- Add : 挙動追加時に検索できるように対応。
- Add : CalcAnimatorParameter追加。
- Add : AnimatorStateTransition追加。
- Add : 遷移線を右クリックで遷移元と遷移先へ移動できるように追加。
- Fix : Prefab元に挙動追加するとPrefab先に正しく追加されないのを修正。
- Other : ForceTransitionをGoToTransitionに改名。
- Other : 挙動追加で表示される組み込みBehaviourの名前を省略しないように変更。
- Other : 組み込みBehaviourをAdd Componentに表示しないように変更。

Ver 1.7.1:
- Add : ステートリストを追加。
- Add : ParamneterReferenceのPropertyDrawerを追加。
- Add : 要素の削除ができるリスト用のGUI、ListGUIを追加。
- Fix : CalcParameterのboolValueがintになっていたのを修正。

Ver 1.7.0;
- Add : パラメータコンテナ。
- Fix : OnStateBegin()で状態遷移した場合、それより下のBehaviourを実行しないように修正。

Ver 1.6.3f1:
- Unity5 RC3でエラーが出るのを修正。
- Unity5 RC3対応により、OnStateEnter/OnStateExitをOnStateBegin/OnStateEndに改名。

Ver 1.6.3:
- Transitionにforceフラグを追加。trueにすると呼び出し時にその場で遷移するようにできる。
- ソースコードへドキュメントコメント埋め込み。
  Player Settings の Scripting Define Symbols に ARBOR_DOC_JA を追加すると日本語でドキュメントコメントが見れるようになります。
- スクリプトリファレンスをAssets/Arbor/Docsに配置。
  解凍してindex.htmlを開いてください。

Ver 1.6.2:
- FIX : OnStateEnterでステート遷移できないのを修正。

Ver 1.6.1:
- FIX : Mac環境でGridボタン押すとエラーが表示される。

Ver 1.6:
- ADD : 常駐ステート。
- ADD : 多言語対応。
- ADD : ArborFSMに名前を付けられるように対応。
- FIX : グリッドサイズを変更してもスナップ間隔に反映されない。
- FIX : ArborFSMのコンポーネントをコピー＆ペーストした際にStateBehaviourが消失する問題の対処。
- FIX : SendTriggerを現在有効なステートにのみ送るように変更。
- FIX : ArborFSMを無効にしてもStateBehaviourが動き続ける。

Ver 1.5:
- ADD : ステートの複数選択に対応。
- ADD : ショートカットキーに対応。
- ADD : グリッド表示対応。
- FIX : Behaviour追加時にデフォルトで広げた状態にする。
- FIX : StateLinkのドラッグ中にステートへのマウスオーバーがずれて反応する。
 
ver 1.4:
- ADD : Tween系Behaviour追加。
 - Tween / Color
 - Tween / Position
 - Tween / Rotation
 - Tween / Scale
- ADD : Add Behaviourに表示されないようにするHideBehaviour属性追加。
- ADD : Behaviourのヘルプボタンから組み込みBehaviourのオンラインヘルプ表示。

ver 1.3:
- ADD : 組み込みBehaviour追加。
 - Audio / PlaySoundAtPoint
 - GameObject / SendMessage
 - Scene / LoadLevel
 - Transition / Force
- ADD : シーンを跨いだコピー&ペースト。
- FIX : Stateをコピーしたあとシーンを保存するとメモリリークの警告が表示される。
- FIX : StateLinkの接続ドラッグ中に画面スクロールすると矢印が残る。

ver 1.2:
- ADD : StateBehaviourの有効チェックボックス。
- FIX : Arbor Editorの最大化を解除するとエラーが出る。
- FIX : 生成したC#スクリプトを編集すると改行コードの警告が出る。

ver 1.1:
- ADD : JavaScriptとBooのスクリプト生成。
- ADD : Stateのコピー＆ペースト。
- ADD : StateBehaviourのコピー＆ペースト。
- FIX : スクリプトがMissingになったときの対応。
- FIX : StateLinkの配列が表示されないのを修正。

ver 1.0.1:
- FIX : Unity4.5でのエラー。
- FIX : エディタ上での実行時にArbor Editorが再描画されない。
- FIX : ArborFSMのInspector拡張のクラス名。
